**MyCarApp**

[https://gregfreeman.me/projects/mycarapp/](https://gregfreeman.me/projects/mycarapp/)

---

- Progressive Web App (PWA)
- HTML, CSS, JS, Ionic Framework

---

**Description**

MyCarApp is a personal project I recently started working on. The idea was to create an app where users can upload details about their car such as specifications, vehicle history, photos, videos, etc. Users will be able to create a QR code sticker that they can place on their car.

This will allow people to easily scan the code and see all of the car details. From there they can get in touch with the owner, share the page, or bookmark the page for later viewing.

![QR Code Sticker](https://i.imgur.com/teEdyvY.jpg)

![IOS App](https://i.imgur.com/mNOvmKY.jpg)

angular.module('demo', ['ionic'])
	.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
		$stateProvider.state('root', {
				url: '/root',
				templateUrl: 'root.html',
				controller: 'RootPageController'
			})
			.state('page', {
				url: '/page',
				templateUrl: 'page-abstract.html',
				abstract: true,
				controller: 'PageController'
			})
			.state('page.details', {
				url: '/details',
				views: {
					'page': {
						templateUrl: 'page-details.html',
						controller: 'PageDetailsPageController'
					}
				}
			})
			.state('page.details-single', {
				url: '/details-single',
				views: {
					'page': {
						templateUrl: 'page-details-single.html',
						controller: 'PageDetailsSinglePageController'
					}
				}
			})
			.state('page.photos', {
				url: '/photos',
				views: {
					'page': {
						templateUrl: 'page-photos.html',
						controller: 'PagePhotosPageController'
					}
				}
			})
			.state('page.videos', {
				url: '/videos',
				views: {
					'page': {
						templateUrl: 'page-videos.html',
						controller: 'PageVideosPageController'
					}
				}
			})
		$urlRouterProvider.otherwise('/root');
	}])
	.controller('RootPageController', function($scope, $ionicSideMenuDelegate) {})
	.controller('NavController', function($scope, $ionicSideMenuDelegate) {
		$scope.toggleLeft = function() {
			$ionicSideMenuDelegate.toggleLeft();
		};
	})
	.controller('FstController', function($scope, $ionicSideMenuDelegate) {})
	.controller('FstHomePageController', function($scope, $ionicSideMenuDelegate) {})
	.controller('FstFirstPageController', function($scope, $ionicSideMenuDelegate) {})
	.controller('FstSecondPageController', function($scope, $ionicSideMenuDelegate) {})
	.controller('PageController', function($scope, $ionicSideMenuDelegate) {})
	.controller('PageDetailsPageController', function($scope, $ionicSideMenuDelegate) {})
	.controller('PagePhotosPageController', function($scope, $ionicSideMenuDelegate) {})
	.controller('PageVideosPageController', function($scope, $ionicSideMenuDelegate) {})
	.controller('MainCtrl', function($scope) {
		$scope.settingsList = [{
			checked: false
		}];
	})

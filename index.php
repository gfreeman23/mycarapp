<!DOCTYPE html>
<html lang="en" ng-app="demo">
<head>
  <script>
    if('VehicleWorker' in navigator) {
      navigator.serviceWorker
               .register('./js/service-worker.js')
               .then(function() { console.log("Service Worker Registered"); });
    }
  </script>
  <noscript key="noscript">Your browser does not support JavaScript!</noscript>
  <title>MyCarApp</title>
  <meta charset="UTF-8">
  <meta name="theme-color" content="#4a87ee">
  <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
  <meta name="msapplication-TileImage" content="./img/favicon.png" />
  <meta name="color-scheme" content="light dark" />
  <link rel="manifest" href="./manifest.json" />
  <link rel="icon" href="./img/favicon.png" sizes="192x192" />
  <link rel="apple-touch-icon" href="./img/favicon.png" />
  <link rel="stylesheet" href="./css/style.css" />
  <link rel="stylesheet" href="./css/ionic.css" />
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@ionic/core@4.7.4/css/ionic.bundle.css"/>
  <script src="https://cdn.jsdelivr.net/npm/@ionic/core@4.7.4/dist/ionic/ionic.esm.js" type="module"></script>
  <script src="./js/ionic.js"></script>
</head>

<body class="" ng-controller="MainCtrl">

  <!-- ALL VIEW STATES LOADED IN HERE -->
  <ion-nav-view></ion-nav-view>

  <script id="page-abstract.html" type="text/ng-template">
    <ion-side-menus>
      <ion-side-menu-content ng-controller="NavController">
        <ion-nav-bar class="bar color-new nav-title-slide-ios7">
          <ion-nav-back-button class="button-clear"><i class="icon ion-arrow-left-c"></i></ion-nav-back-button>
        </ion-nav-bar>
        <ion-nav-view name="page" animation="slide-left-right"></ion-nav-view>
      </ion-side-menu-content>
    </ion-side-menus>
  </script>

  <script id="root.html" type="text/ng-template">
    <ion-view>
      <ion-nav-buttons side="left">
        <button menu-toggle="left" class="button button-icon icon ion-navicon"></button>
      </ion-nav-buttons>
      <ion-content has-header="true" padding="true">
        <div class="content-wrapper">
          <div class="avatar-wrap">
            <ion-avatar style="width: 150px; height: 150px; margin: 20px auto 0 auto;">
              <img src="./img/profile-pic.jpg" style="border: 5px solid #323a42;" />
            </ion-avatar>
          </div>
          <ion-item lines="none" style="text-align: center;">
            <ion-label>
              <h1>John Smith</h1>
              <p><ion-icon name="ios-pin" style="color: #323a42; vertical-align: middle; margin-bottom: 4px;"></ion-icon></i> Los Angeles, California</p>
            </ion-label>
          </ion-item>
          <ion-item lines="none" style="text-align: center;">
            <ion-label>
              <h1>My Garage</h1>
              <p><a href="#/page/details">1958 Porsche 356A Speedster</a></p>
            </ion-label>
          </ion-item>
          <ion-item lines="none" style="text-align: center;">
            <ion-label>
              <h1>About Me</h1>
              <p>My name is John and I work for one the Largest Air-Cooled Porsche Dealer in the US with over 70 Air-Cooled Porsche's at all times. We also provide other exotic vehicles from Ferrari's to Lamborghini's. We can assist in finding you the vehicle you are looking for and also assist in shipping worldwide.</p>
            </ion-label>
          </ion-item>
          <ion-item lines="none" style="text-align: center;">
            <ion-label>
              <h1>Contact Info</h1>
            </ion-label>
          </ion-item>
          <ion-item lines="none">
            <a href="" class="button color-new" style="width: 100%; max-width: 400px; margin: 0 auto 10px auto;"><ion-icon name="logo-instagram"></ion-icon> Instagram</a>
          </ion-item>
          <ion-item lines="none">
            <a href="" class="button color-new" style="width: 100%; max-width: 400px; margin: 0 auto 10px auto;"><ion-icon name="logo-facebook"></ion-icon> Facebook</a>
          </ion-item>
          <ion-item lines="none">
            <a href="" class="button color-new" style="width: 100%; max-width: 400px; margin: 0 auto 10px auto;"><ion-icon name="ios-mail"></ion-icon> Email</a>
          </ion-item>
          <ion-item lines="none">
            <a href="" class="button color-new" style="width: 100%; max-width: 400px; margin: 0 auto 10px auto;"><ion-icon name="globe"></ion-icon></i> Website</a>
          </ion-item>
          <ion-row style="height: 15px"></ion-row>
        </div>
        <ion-row style="height: 90px"></ion-row>
      </ion-content>
      <div class="color-new tabs tabs-icon-top">
        <a nav-clear class="tab-item disable-user-behavior active" title="Home" nav-clear ui-sref="root">
          <i class="icon ion-home"></i><span class="tab-title">Home</span>
        </a>
        <a nav-clear class="tab-item disable-user-behavior" title="Car Details" icon-on="ion-ios7-filing" icon-off="ion-ios7-filing-outline" ui-sref="page.details">
          <i class="icon ion-model-s"></i><span class="tab-title">Car Details</span>
        </a>
        <a nav-clear class="tab-item disable-user-behavior" title="Photos" icon-on="ion-ios7-filing" icon-off="ion-ios7-filing-outline" ui-sref="page.photos">
          <i class="icon ion-android-camera"></i><span class="tab-title">Photos</span>
        </a>
        <a nav-clear class="tab-item disable-user-behavior" title="Videos" icon-on="ion-ios7-filing" icon-off="ion-ios7-filing-outline" ui-sref="page.videos">
          <i class="icon ion-film-marker"></i><span class="tab-title">Videos</span>
        </a>
      </div>
    </ion-view>
  </script>

  <script id="page-details.html" type="text/ng-template">
    <ion-view title="Details">
      <ion-content has-header="true" padding="true" overflow-scroll="true">
        <div class="content-wrapper">
          <div class="full-width"><img src="./img/1.jpg"/></div>
          <ion-item>
            <ion-chip class="color-new">
              <a href="#/page/details-single"><i class="icon ion-clock"></i> Vehicle History</a>
            </ion-chip>
            <ion-chip class="color-new">
              <a href="#/page/photos"><i class="icon ion-android-camera"></i> Photos</a>
            </ion-chip>
            <ion-chip class="color-new">
              <a href="#/page/videos"><i class="icon ion-film-marker"></i> Videos</a>
            </ion-chip>
          </ion-item>
          <ion-item>
           <ion-avatar slot="start">
             <img src="./img/logo-porsche.png">
           </ion-avatar>
           <ion-label>
             <h1>1958 Porsche 356A Speedster</h1>
           </ion-label>
          </ion-item>
          <ion-row style="height: 15px"></ion-row>
          <ion-item lines="none">
            <div class="list">
              <div class="item">
                <p><strong>Exterior</strong><br>
                  - Polyantha Red Paint</p>
                <p><strong>Interior</strong><br>
                  - Black Upholstery<br>
                  - Fixed-Back buckets<br>
                  - Three-spoke VDM steering wheel</p>
                <p><strong>Wheels/Tires</strong><br>
                  - 15" Wheels<br>
                  - 205/50 Dunlop Direzza Zill tires</p>
                <p><strong>Engine</strong><br>
                  - 1.6-liter flat four from a Porsche 912<br>
                  - 200mm flywheel</p>
                <p><strong>Suspension</strong><br>
                  - Koni Classic shocks<br>
                  - 356C four-wheel discs</p>
                <p><strong>Exhaust</strong><br>
                  - Bursch stainless exhaust system</p>
                <p><strong>Description</strong><br>
                  This 1958 Porsche 356A Speedster was delivered new in Augsburg, Germany to an English school teacher in late 1957 and was later imported to the US, where it was purchased in 1992 by racing driver and Willow Springs raceway founder Robert Kirby. It was later acquired by the current owner after being auctioned on BaT in January 2019.</p>
                <p>Power is provided by a 912-sourced 1.6-liter flat-four paired with a four-speed manual transaxle, and equipment includes a black soft top, 15″ wheels, 356C four-wheel disc brakes, a 200mm flywheel and clutch, and a Bursch stainless exhaust system.</p>
                <p>Service performed in preparation for the sale included an oil change and brake fluid flush along with performing a compression test. This 356A Speedster is now offered on dealer consignment with its original engine, a driver’s manual, a copy of the Kardex report, and a clean Oregon title.</p>
              </div>
            </div>
          </ion-item>
          <ion-row style="height: 15px"></ion-row>
        </div>
        <ion-row style="height: 90px"></ion-row>
      </ion-content>
      <div class="color-new tabs tabs-icon-top">
        <a nav-clear class="tab-item disable-user-behavior" title="Home" nav-clear ui-sref="root">
          <i class="icon ion-home"></i><span class="tab-title">Home</span>
        </a>
        <a nav-clear class="tab-item disable-user-behavior active" title="Car Details" icon-on="ion-ios7-filing" icon-off="ion-ios7-filing-outline" ui-sref="page.details">
          <i class="icon ion-model-s"></i><span class="tab-title">Car Details</span>
        </a>
        <a nav-clear class="tab-item disable-user-behavior" title="Photos" icon-on="ion-ios7-filing" icon-off="ion-ios7-filing-outline" ui-sref="page.photos">
          <i class="icon ion-android-camera"></i><span class="tab-title">Photos</span>
        </a>
        <a nav-clear class="tab-item disable-user-behavior" title="Excercise" icon-on="ion-ios7-filing" icon-off="ion-ios7-filing-outline" ui-sref="page.videos">
          <i class="icon ion-film-marker"></i><span class="tab-title">Videos</span>
        </a>
      </div>
    </ion-view>
  </script>

  <script id="page-details-single.html" type="text/ng-template">
    <ion-view title="Vehicle History">
      <ion-content has-header="false" padding="true" overflow-scroll="true">
        <ion-row style="height: 45px"></ion-row>
        <div class="content-wrapper">
          <ion-list>
             <ion-list-header>Vehicle History</ion-list-header>
             <ion-item>
               <ion-avatar slot="start">
                 <img src="./img/logo-porsche.png">
               </ion-avatar>
               <ion-label>
                 <p><strong>7/9/19</strong> - 30,965 miles</p>
                 <p>Porsche Newport Beach</p>
                 <p>Purchased</p>
               </ion-label>
             </ion-item>
             <ion-item>
               <ion-avatar slot="start">
                 <img src="./img/logo-tint.png">
               </ion-avatar>
               <ion-label>
                 <p><strong>7/20/19</strong> - 31,200 miles</p>
                 <p>The Tint Zone</p>
                 <p>CTX15 and CTX35 Tint Installed</p>
               </ion-label>
             </ion-item>
             <ion-item>
               <ion-avatar slot="start">
                 <img src="./img/logo-porsche.png">
               </ion-avatar>
               <ion-label>
                 <p><strong>8/7/19</strong> - 31,623 miles</p>
                 <p>Porsche Newport Beach</p>
                 <p>Installed M Performance Power and Sound Kit</p>
               </ion-label>
             </ion-item>
             <ion-item lines="none">
               <ion-avatar slot="start">
                 <img src="./img/logo-porsche.png">
               </ion-avatar>
               <ion-label>
                 <p><strong>8/21/19</strong> - 33,360 miles</p>
                 <p>Porsche Newport Beach</p>
                 <p>Replaced Broken E Brake Cable</p>
               </ion-label>
             </ion-item>
          </ion-list>
        </div>
      </ion-content>
    </ion-view>
  </script>

  <script id="page-photos.html" type="text/ng-template">
    <ion-view title="Photos">
      <ion-content has-header="true" padding="true" overflow-scroll="true">
        <div class="list list-gallery">
          <div class="item item-thumbnail">
            <img src="./img/1.jpg">
          </div>
          <div class="item item-thumbnail">
            <img src="./img/2.jpg">
          </div>
          <div class="item item-thumbnail">
            <img src="./img/3.jpg">
          </div>
          <div class="item item-thumbnail">
            <img src="./img/4.jpg">
          </div>
          <div class="item item-thumbnail">
            <img src="./img/5.jpg">
          </div>
          <div class="item item-thumbnail">
            <img src="./img/6.jpg">
          </div>
          <div class="item item-thumbnail">
            <img src="./img/7.jpg">
          </div>
          <div class="item item-thumbnail">
            <img src="./img/8.jpg">
          </div>
          <div class="item item-thumbnail">
            <img src="./img/9.jpg">
          </div>
          <div class="item item-thumbnail">
            <img src="./img/10.jpg">
          </div>
          <div class="item item-thumbnail">
            <img src="./img/11.jpg">
          </div>
          <div class="item item-thumbnail">
            <img src="./img/12.jpg">
          </div>
          <div class="item item-thumbnail">
            <img src="./img/13.jpg">
          </div>
          <div class="item item-thumbnail">
            <img src="./img/14.jpg">
          </div>
          <div class="item item-thumbnail">
            <img src="./img/15.jpg">
          </div>
          <div class="item item-thumbnail">
            <img src="./img/16.jpg">
          </div>
          <div class="item item-thumbnail">
            <img src="./img/17.jpg">
          </div>
        </div>
        <ion-row style="height: 70px"></ion-row>
      </ion-content>
      <div class="color-new tabs tabs-icon-top">
        <a nav-clear class="tab-item disable-user-behavior" title="Home" nav-clear ui-sref="root">
          <i class="icon ion-home"></i><span class="tab-title">Home</span>
        </a>
        <a nav-clear class="tab-item disable-user-behavior" title="Car Details" icon-on="ion-ios7-filing" icon-off="ion-ios7-filing-outline" ui-sref="page.details">
          <i class="icon ion-model-s"></i><span class="tab-title">Car Details</span>
        </a>
        <a nav-clear class="tab-item disable-user-behavior active" title="Photos" icon-on="ion-ios7-filing" icon-off="ion-ios7-filing-outline" ui-sref="page.photos">
          <i class="icon ion-android-camera"></i><span class="tab-title">Photos</span>
        </a>
        <a nav-clear class="tab-item disable-user-behavior" title="Excercise" icon-on="ion-ios7-filing" icon-off="ion-ios7-filing-outline" ui-sref="page.videos">
          <i class="icon ion-film-marker"></i><span class="tab-title">Videos</span>
        </a>
      </div>
    </ion-view>
  </script>

  <script id="page-videos.html" type="text/ng-template">
    <ion-view title="Videos">
      <ion-content has-header="true" padding="true" overflow-scroll="true">
        <div class="list list-gallery">
          <div class="item item-thumbnail">
            <div class="video-wrapper">
              <iframe width="560" height="315" src="https://www.youtube.com/embed/__3OdHahU-w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
          </div>
          <div class="item item-thumbnail">
            <div class="video-wrapper">
              <iframe width="560" height="315" src="https://www.youtube.com/embed/ro6fsJL8p3Q" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
          </div>
          <div class="item item-thumbnail">
            <div class="video-wrapper">
              <iframe width="560" height="315" src="https://www.youtube.com/embed/m677iluus_Y" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
          </div>
          <div class="item item-thumbnail">
            <div class="video-wrapper">
              <iframe width="560" height="315" src="https://www.youtube.com/embed/HottEnrmQss" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
          </div>
        </div>
        <ion-row style="height: 70px"></ion-row>
      </ion-content>
      <div class="color-new tabs tabs-icon-top">
        <a nav-clear class="tab-item disable-user-behavior" title="Home" nav-clear ui-sref="root">
          <i class="icon ion-home"></i><span class="tab-title">Home</span>
        </a>
        <a nav-clear class="tab-item disable-user-behavior" title="Car Details" icon-on="ion-ios7-filing" icon-off="ion-ios7-filing-outline" ui-sref="page.details">
          <i class="icon ion-model-s"></i><span class="tab-title">Car Details</span>
        </a>
        <a nav-clear class="tab-item disable-user-behavior" title="Photos" icon-on="ion-ios7-filing" icon-off="ion-ios7-filing-outline" ui-sref="page.photos">
          <i class="icon ion-android-camera"></i><span class="tab-title">Photos</span>
        </a>
        <a nav-clear class="tab-item disable-user-behavior active" title="Videos" icon-on="ion-ios7-filing" icon-off="ion-ios7-filing-outline" ui-sref="page.videos">
          <i class="icon ion-film-marker"></i><span class="tab-title">Videos</span>
        </a>
      </div>
    </ion-view>
  </script>

  <script  src="./js/script.js"></script>

</body>
</html>
